from telegram.ext import ApplicationBuilder
from telegram.constants import ParseMode
from dotenv import load_dotenv
from os import getenv

load_dotenv()

bot_token = getenv('BOT_TOKEN')

class Bot:
  def __init__(self, bot_token):
    self.app = ApplicationBuilder().token(bot_token).build()

  async def send_text(self, chat_id, text):
    message = await self.app.bot.send_message(chat_id, text, parse_mode=ParseMode.MARKDOWN_V2, disable_web_page_preview=True)
    return message

  async def get_member(self, chat_id, user_id):
    member = await self.app.bot.get_chat_member(chat_id=chat_id, user_id=user_id)
    return member

bot = Bot(bot_token)
