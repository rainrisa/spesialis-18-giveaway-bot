class Config:
  def __init__(self):
    self.total_members_added_req = 2
    self.password_length = 5
    self.delete_message_timeout = 60 # in seconds
    self.giveaway_preview_url = "https://t.me/achapremium"
    self.giveaway_url = "https://t.me/berbagivid18"

config = Config()
