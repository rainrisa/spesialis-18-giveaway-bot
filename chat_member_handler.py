from telegram import Update
from telegram.helpers import escape_markdown
from telegram.ext import ContextTypes
from store import invitation_store, password_store, giveaway_store
from config import config
from generate_password import generate_password
from mention import mention
from set_timeout import set_timeout
from bot import bot

async def chat_member_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
  chat_member = update.chat_member
  chat_id = chat_member.chat.id
  adder = chat_member.from_user
  new_user = chat_member.new_chat_member.user
  new_status = chat_member.new_chat_member.status
  old_status = chat_member.old_chat_member.status
  accepted_old_status = {'left', 'kicked'}

  valid_invitation = adder.id != new_user.id
  valid_member = new_status == "member" and old_status in accepted_old_status and new_user.is_bot != True  

  if valid_invitation and valid_member:
    tag_adder = mention(adder)
    tag_new_user = mention(new_user)
    new_user_exist = invitation_store.get_by_value(new_user.id)
    text = "p" # to prevent text become local variable

    if new_user_exist:
      text = f'maaf {tag_adder}\n\n{tag_new_user} sudah pernah ditambahkan\nsilahkan cari member lain yang belum pernah join kesini'

    else:
      invitation_store.add(adder.id, [new_user.id])
      total_members_added = len(invitation_store.get_by_key(adder.id))

      if total_members_added >= config.total_members_added_req:
        already_got_giveaway = giveaway_store.get_by_key(adder.id)[0]

        if already_got_giveaway: return

        bot_username = update._bot.username
        password = password_store.get_by_key(adder.id)[0]

        if password is None:
          password = generate_password()
          password_store.add(adder.id, [password])

        link = escape_markdown(f'https://t.me/{bot_username}?start={password}', version=2)
        text = f'ok {tag_adder}, ambil vidnya disini:\n{link}'

        giveaway_store.add(adder.id, [True])
      
      else:
        remaining_members = config.total_members_added_req - total_members_added
        text = f'berhasil menambahkan {tag_new_user}\n\n{tag_adder}, kamu harus menambahkan {remaining_members} member lagi\nsebelum bisa [mendapatkan vidnya]({config.giveaway_preview_url})'

    reply_message = await bot.send_text(chat_id, text)
    set_timeout(reply_message.delete, config.delete_message_timeout)
  