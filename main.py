from telegram import Update
from telegram.ext import CommandHandler, ChatMemberHandler
from start_handler import start_handler
from test_handler import test_handler
from chat_member_handler import chat_member_handler
from bot import bot

app = bot.app

app.add_handler(CommandHandler("start", start_handler))
app.add_handler(ChatMemberHandler(chat_member_handler, ChatMemberHandler.CHAT_MEMBER))

app.run_polling(allowed_updates=Update.ALL_TYPES)
