import asyncio

def set_timeout(task: callable, seconds = 5):
  async def schedule():
    await asyncio.sleep(seconds)

    if asyncio.iscoroutinefunction(task):
      await task()
    else:
      task()

  asyncio.ensure_future(schedule())

