class Store:
  def __init__(self):
    self.data = {}

  def add(self, key, values):
    if not isinstance(key, int) or not isinstance(values, list):
      raise ValueError("Number must be an integer and value must be a list")
    
    if key in self.data:
      self.data[key].extend(values)
    else:
      self.data[key] = values

  def get_by_value(self, value):
    for key, data_value in self.data.items():
      if value in data_value:
        return key

    return None

  def get_by_key(self, key):
    return self.data.get(key, [None])

  def delete(self, key):
    if key in self.data:
      del self.data[key]

invitation_store = Store()
password_store = Store()
giveaway_store = Store()
