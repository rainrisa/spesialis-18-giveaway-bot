class User:
  def __init__(self, id: int, first_name: str):
    self.id = id
    self.first_name = first_name

def mention(user: User):
  return f"[{user.first_name}](tg://user?id={user.id})"