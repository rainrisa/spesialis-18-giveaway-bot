from telegram import Update
from telegram.helpers import escape_markdown
from telegram.ext import ContextTypes
from bot import bot
from store import password_store
from config import config

async def start_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
  chat_id = update.message.chat_id
  user_id = update.message.from_user.id
  password = context.args[0]
  saved_password = password_store.get_by_key(user_id)[0]

  if password == saved_password:
    vid_url = escape_markdown(config.giveaway_url, version=2)
    await bot.send_text(chat_id, f'ini link grup berisi vidnya:\n{vid_url}')
    password_store.delete(user_id)
  else:
    await bot.send_text(chat_id, 'maaf kamu tidak terdaftar')
  
  return
