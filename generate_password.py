from config import config
import random

def generate_password():
  return ''.join(random.choices("bcdfghjklmnpqrstvwxyz", k=config.password_length))
